//========================================================================================================================================================================================================200
//	DEFINE/INCLUDE
//========================================================================================================================================================================================================200

//======================================================================================================================================================150
//	MAIN FUNCTION HEADER
//======================================================================================================================================================150

#include "./../main.h"								// (in the main program folder)

//======================================================================================================================================================150
//	DEFINE
//======================================================================================================================================================150

//======================================================================================================================================================150
//	LIBRARIES
//======================================================================================================================================================150

#include <stdio.h>									// (in path known to compiler)	needed by printf
#include <string.h>									// (in path known to compiler)	needed by strlen

#include <CL/cl.h>									// (in path specified to compiler)			needed by OpenCL types and functions

#include "./../../../common/simple-opencl/simpleCL.h"
//======================================================================================================================================================150
//	UTILITIES
//======================================================================================================================================================150

#include "./../util/opencl/opencl.h"				// (in directory)							needed by device functions

//======================================================================================================================================================150
//	KERNEL_GPU_CUDA_WRAPPER FUNCTION HEADER
//======================================================================================================================================================150

#include "./kernel_gpu_opencl_wrapper.h"			// (in directory)

//======================================================================================================================================================150
//	END
//======================================================================================================================================================150

//========================================================================================================================================================================================================200
//	KERNEL_GPU_CUDA_WRAPPER FUNCTION
//========================================================================================================================================================================================================200

void kernel_gpu_opencl_wrapper( fp* image,						// input image
		int Nr,												// IMAGE nbr of rows
		int Nc,												// IMAGE nbr of cols
		long Ne,											// IMAGE nbr of elem
		int niter,											// nbr of iterations
		fp lambda,											// update step size
		long NeROI,										// ROI nbr of elements
		int* iN, int* iS, int* jE, int* jW, int iter,			// primary loop
		int mem_size_i, int mem_size_j) {

	//======================================================================================================================================================150
	//	GPU SETUP
	//======================================================================================================================================================150

	//====================================================================================================100
	//	COMMON VARIABLES
	//====================================================================================================100

	// common variables
	cl_int error;
	char stdPath[]="./kernel/kernel_gpu_opencl.cl";

	sclHard* hardware_list;
	sclHard hardware;

	sclSoft extract_software, prepare_software, reduce_software, srad_software,
			srad2_software, compress_software;
	int found;

	//====================================================================================================100
	//	SAMPLE OPEN CL CALLS
	//====================================================================================================100

	//Get the hardware
	hardware_list=sclGetAllHardware(&found);
	hardware=hardware_list[0];

	//Get the software
	extract_software = sclGetCLSoftware(stdPath,
			"extract_kernel", hardware);
	prepare_software = sclGetCLSoftware(stdPath,
			"prepare_kernel", hardware);
	reduce_software = sclGetCLSoftware(stdPath,
			"reduce_kernel", hardware);
	srad_software = sclGetCLSoftware(stdPath,
			"srad_kernel", hardware);
	srad2_software = sclGetCLSoftware(stdPath,
			"srad2_kernel", hardware);
	compress_software = sclGetCLSoftware(stdPath,
			"compress_kernel", hardware);

	//====================================================================================================100
	//	TRIGGERING INITIAL DRIVER OVERHEAD
	//====================================================================================================100
	// cudaThreadSynchronize();		// the above does it
	//======================================================================================================================================================150
	// 	GPU VARIABLES
	//======================================================================================================================================================150
	// CUDA kernel execution parameters
	int blocks_x;

	//======================================================================================================================================================150
	// 	ALLOCATE MEMORY IN GPU
	//======================================================================================================================================================150

	//====================================================================================================100
	// common memory size
	//====================================================================================================100

	int mem_size;										// matrix memory size
	mem_size = sizeof(fp) * Ne;	// get the size of float representation of input IMAGE

	//====================================================================================================100
	// allocate memory for entire IMAGE on DEVICE
	//====================================================================================================100

	cl_mem d_I;

	d_I = clCreateBuffer(hardware.context, CL_MEM_READ_WRITE, mem_size, NULL,
			&error);
	if (error != CL_SUCCESS)
		fatal_CL(error, __LINE__);

	//====================================================================================================100
	// allocate memory for coordinates on DEVICE
	//====================================================================================================100

	cl_mem d_iN;
	d_iN = clCreateBuffer(hardware.context, CL_MEM_READ_WRITE, mem_size_i, NULL,
			&error);
	if (error != CL_SUCCESS)
		fatal_CL(error, __LINE__);

	cl_mem d_iS;
	d_iS = clCreateBuffer(hardware.context, CL_MEM_READ_WRITE, mem_size_i, NULL,
			&error);
	if (error != CL_SUCCESS)
		fatal_CL(error, __LINE__);

	cl_mem d_jE;
	d_jE = clCreateBuffer(hardware.context, CL_MEM_READ_WRITE, mem_size_j, NULL,
			&error);
	if (error != CL_SUCCESS)
		fatal_CL(error, __LINE__);

	cl_mem d_jW;
	d_jW = clCreateBuffer(hardware.context, CL_MEM_READ_WRITE, mem_size_j, NULL,
			&error);
	if (error != CL_SUCCESS)
		fatal_CL(error, __LINE__);

	//====================================================================================================100
	// allocate memory for derivatives
	//====================================================================================================100

	cl_mem d_dN;
	d_dN = clCreateBuffer(hardware.context, CL_MEM_READ_WRITE, mem_size, NULL,
			&error);
	if (error != CL_SUCCESS)
		fatal_CL(error, __LINE__);

	cl_mem d_dS;
	d_dS = clCreateBuffer(hardware.context, CL_MEM_READ_WRITE, mem_size, NULL,
			&error);
	if (error != CL_SUCCESS)
		fatal_CL(error, __LINE__);

	cl_mem d_dW;
	d_dW = clCreateBuffer(hardware.context, CL_MEM_READ_WRITE, mem_size, NULL,
			&error);
	if (error != CL_SUCCESS)
		fatal_CL(error, __LINE__);

	cl_mem d_dE;
	d_dE = clCreateBuffer(hardware.context, CL_MEM_READ_WRITE, mem_size, NULL,
			&error);
	if (error != CL_SUCCESS)
		fatal_CL(error, __LINE__);

	//====================================================================================================100
	// allocate memory for coefficient on DEVICE
	//====================================================================================================100

	cl_mem d_c;
	d_c = clCreateBuffer(hardware.context, CL_MEM_READ_WRITE, mem_size, NULL,
			&error);
	if (error != CL_SUCCESS)
		fatal_CL(error, __LINE__);

	//====================================================================================================100
	// allocate memory for partial sums on DEVICE
	//====================================================================================================100

	cl_mem d_sums;

	d_sums = clCreateBuffer(hardware.context, CL_MEM_READ_WRITE, mem_size, NULL,
			&error);
	if (error != CL_SUCCESS)
		fatal_CL(error, __LINE__);

	cl_mem d_sums2;

	d_sums2 = clCreateBuffer(hardware.context, CL_MEM_READ_WRITE, mem_size,
			NULL, &error);
	if (error != CL_SUCCESS)
		fatal_CL(error, __LINE__);

	//====================================================================================================100
	// End
	//====================================================================================================100

	//======================================================================================================================================================150
	// 	COPY INPUT TO CPU
	//======================================================================================================================================================150

	//====================================================================================================100
	// Image
	//====================================================================================================100

	error = clEnqueueWriteBuffer(hardware.queue, d_I, 1, 0, mem_size, image, 0,
			0, 0);
	if (error != CL_SUCCESS)
		fatal_CL(error, __LINE__);

	//====================================================================================================100
	// coordinates
	//====================================================================================================100

	error = clEnqueueWriteBuffer(hardware.queue, d_iN, 1, 0, mem_size_i, iN, 0,
			0, 0);
	if (error != CL_SUCCESS)
		fatal_CL(error, __LINE__);

	error = clEnqueueWriteBuffer(hardware.queue, d_iS, 1, 0, mem_size_i, iS, 0,
			0, 0);
	if (error != CL_SUCCESS)
		fatal_CL(error, __LINE__);

	error = clEnqueueWriteBuffer(hardware.queue, d_jE, 1, 0, mem_size_j, jE, 0,
			0, 0);
	if (error != CL_SUCCESS)
		fatal_CL(error, __LINE__);

	error = clEnqueueWriteBuffer(hardware.queue, d_jW, 1, 0, mem_size_j, jW, 0,
			0, 0);
	if (error != CL_SUCCESS)
		fatal_CL(error, __LINE__);

	//====================================================================================================100
	// End
	//====================================================================================================100

	//======================================================================================================================================================150
	// 	KERNEL EXECUTION PARAMETERS
	//======================================================================================================================================================150

	// threads
	size_t local_work_size[1];
	local_work_size[0] = NUMBER_THREADS;

	// workgroups
	int blocks_work_size;
	size_t global_work_size[1];
	blocks_x = Ne / (int) local_work_size[0];
	if (Ne % (int) local_work_size[0] != 0) {// compensate for division remainder above by adding one grid
		blocks_x = blocks_x + 1;
	}
	blocks_work_size = blocks_x;
	global_work_size[0] = blocks_work_size * local_work_size[0];// define the number of blocks in the grid

	printf(
			"max # of workgroups = %d, # of threads/workgroup = %d (ensure that device can handle)\n",
			(int) (global_work_size[0] / local_work_size[0]),
			(int) local_work_size[0]);

	//======================================================================================================================================================150
	// 	Extract Kernel - SCALE IMAGE DOWN FROM 0-255 TO 0-1 AND EXTRACT
	//======================================================================================================================================================150
	//====================================================================================================100
	//	set arguments
	//====================================================================================================100

	error = clSetKernelArg(extract_software.kernel, 0, sizeof(long),
			(void *) &Ne);
	if (error != CL_SUCCESS)
		fatal_CL(error, __LINE__);
	error = clSetKernelArg(extract_software.kernel, 1, sizeof(cl_mem),
			(void *) &d_I);
	if (error != CL_SUCCESS)
		fatal_CL(error, __LINE__);

	//====================================================================================================100
	//	launch kernel
	//====================================================================================================100

	error = clEnqueueNDRangeKernel(hardware.queue, extract_software.kernel, 1, NULL,
			global_work_size, local_work_size, 0, NULL, NULL);
	if (error != CL_SUCCESS)
		fatal_CL(error, __LINE__);

	//====================================================================================================100
	//	Synchronization - wait for all operations in the command queue so far to finish
	//====================================================================================================100

	// error = clFinish(hardware.queue);
	// if (error != CL_SUCCESS) 
	// fatal_CL(error, __LINE__);

	//====================================================================================================100
	//	End
	//====================================================================================================100

	//======================================================================================================================================================150
	// 	WHAT IS CONSTANT IN COMPUTATION LOOP
	//======================================================================================================================================================150

	//====================================================================================================100
	//	Prepare Kernel0
	//====================================================================================================100

	error = clSetKernelArg(prepare_software.kernel, 0, sizeof(long),
			(void *) &Ne);
	if (error != CL_SUCCESS)
		fatal_CL(error, __LINE__);
	error = clSetKernelArg(prepare_software.kernel, 1, sizeof(cl_mem),
			(void *) &d_I);
	if (error != CL_SUCCESS)
		fatal_CL(error, __LINE__);
	error = clSetKernelArg(prepare_software.kernel, 2, sizeof(cl_mem),
			(void *) &d_sums);
	if (error != CL_SUCCESS)
		fatal_CL(error, __LINE__);
	error = clSetKernelArg(prepare_software.kernel, 3, sizeof(cl_mem),
			(void *) &d_sums2);
	if (error != CL_SUCCESS)
		fatal_CL(error, __LINE__);

	//====================================================================================================100
	//	Reduce Kernel
	//====================================================================================================100

	int blocks2_x;
	int blocks2_work_size;
	size_t global_work_size2[1];
	long no;
	int mul;
	int mem_size_single = sizeof(fp) * 1;
	// error = clFinish(hardware.queue);
	fp total;
	fp total2;
	fp meanROI;
	fp meanROI2;
	fp varROI;
	fp q0sqr;

	error = clSetKernelArg(reduce_software.kernel, 0, sizeof(long),
			(void *) &Ne);
	if (error != CL_SUCCESS)
		fatal_CL(error, __LINE__);
	error = clSetKernelArg(reduce_software.kernel, 3, sizeof(cl_mem),
			(void *) &d_sums);
	if (error != CL_SUCCESS)
		fatal_CL(error, __LINE__);
	error = clSetKernelArg(reduce_software.kernel, 4, sizeof(cl_mem),
			(void *) &d_sums2);
	if (error != CL_SUCCESS)
		fatal_CL(error, __LINE__);

	//====================================================================================================100
	//	SRAD Kernel
	//====================================================================================================100

	error = clSetKernelArg(srad_software.kernel, 0, sizeof(fp),
			(void *) &lambda);
	if (error != CL_SUCCESS)
		fatal_CL(error, __LINE__);
	error = clSetKernelArg(srad_software.kernel, 1, sizeof(int), (void *) &Nr);
	if (error != CL_SUCCESS)
		fatal_CL(error, __LINE__);
	error = clSetKernelArg(srad_software.kernel, 2, sizeof(int), (void *) &Nc);
	if (error != CL_SUCCESS)
		fatal_CL(error, __LINE__);
	error = clSetKernelArg(srad_software.kernel, 3, sizeof(long), (void *) &Ne);
	if (error != CL_SUCCESS)
		fatal_CL(error, __LINE__);
	error = clSetKernelArg(srad_software.kernel, 4, sizeof(cl_mem),
			(void *) &d_iN);
	if (error != CL_SUCCESS)
		fatal_CL(error, __LINE__);
	error = clSetKernelArg(srad_software.kernel, 5, sizeof(cl_mem),
			(void *) &d_iS);
	if (error != CL_SUCCESS)
		fatal_CL(error, __LINE__);
	error = clSetKernelArg(srad_software.kernel, 6, sizeof(cl_mem),
			(void *) &d_jE);
	if (error != CL_SUCCESS)
		fatal_CL(error, __LINE__);
	error = clSetKernelArg(srad_software.kernel, 7, sizeof(cl_mem),
			(void *) &d_jW);
	if (error != CL_SUCCESS)
		fatal_CL(error, __LINE__);
	error = clSetKernelArg(srad_software.kernel, 8, sizeof(cl_mem),
			(void *) &d_dN);
	if (error != CL_SUCCESS)
		fatal_CL(error, __LINE__);
	error = clSetKernelArg(srad_software.kernel, 9, sizeof(cl_mem),
			(void *) &d_dS);
	if (error != CL_SUCCESS)
		fatal_CL(error, __LINE__);
	error = clSetKernelArg(srad_software.kernel, 10, sizeof(cl_mem),
			(void *) &d_dW);
	if (error != CL_SUCCESS)
		fatal_CL(error, __LINE__);
	error = clSetKernelArg(srad_software.kernel, 11, sizeof(cl_mem),
			(void *) &d_dE);
	if (error != CL_SUCCESS)
		fatal_CL(error, __LINE__);
	error = clSetKernelArg(srad_software.kernel, 13, sizeof(cl_mem),
			(void *) &d_c);
	if (error != CL_SUCCESS)
		fatal_CL(error, __LINE__);
	error = clSetKernelArg(srad_software.kernel, 14, sizeof(cl_mem),
			(void *) &d_I);
	if (error != CL_SUCCESS)
		fatal_CL(error, __LINE__);

	//====================================================================================================100
	//	SRAD2 Kernel
	//====================================================================================================100

	error = clSetKernelArg(srad2_software.kernel, 0, sizeof(fp),
			(void *) &lambda);
	if (error != CL_SUCCESS)
		fatal_CL(error, __LINE__);
	error = clSetKernelArg(srad2_software.kernel, 1, sizeof(int), (void *) &Nr);
	if (error != CL_SUCCESS)
		fatal_CL(error, __LINE__);
	error = clSetKernelArg(srad2_software.kernel, 2, sizeof(int), (void *) &Nc);
	if (error != CL_SUCCESS)
		fatal_CL(error, __LINE__);
	error = clSetKernelArg(srad2_software.kernel, 3, sizeof(long),
			(void *) &Ne);
	if (error != CL_SUCCESS)
		fatal_CL(error, __LINE__);
	error = clSetKernelArg(srad2_software.kernel, 4, sizeof(cl_mem),
			(void *) &d_iN);
	if (error != CL_SUCCESS)
		fatal_CL(error, __LINE__);
	error = clSetKernelArg(srad2_software.kernel, 5, sizeof(cl_mem),
			(void *) &d_iS);
	if (error != CL_SUCCESS)
		fatal_CL(error, __LINE__);
	error = clSetKernelArg(srad2_software.kernel, 6, sizeof(cl_mem),
			(void *) &d_jE);
	if (error != CL_SUCCESS)
		fatal_CL(error, __LINE__);
	error = clSetKernelArg(srad2_software.kernel, 7, sizeof(cl_mem),
			(void *) &d_jW);
	if (error != CL_SUCCESS)
		fatal_CL(error, __LINE__);
	error = clSetKernelArg(srad2_software.kernel, 8, sizeof(cl_mem),
			(void *) &d_dN);
	if (error != CL_SUCCESS)
		fatal_CL(error, __LINE__);
	error = clSetKernelArg(srad2_software.kernel, 9, sizeof(cl_mem),
			(void *) &d_dS);
	if (error != CL_SUCCESS)
		fatal_CL(error, __LINE__);
	error = clSetKernelArg(srad2_software.kernel, 10, sizeof(cl_mem),
			(void *) &d_dW);
	if (error != CL_SUCCESS)
		fatal_CL(error, __LINE__);
	error = clSetKernelArg(srad2_software.kernel, 11, sizeof(cl_mem),
			(void *) &d_dE);
	if (error != CL_SUCCESS)
		fatal_CL(error, __LINE__);
	error = clSetKernelArg(srad2_software.kernel, 12, sizeof(cl_mem),
			(void *) &d_c);
	if (error != CL_SUCCESS)
		fatal_CL(error, __LINE__);
	error = clSetKernelArg(srad2_software.kernel, 13, sizeof(cl_mem),
			(void *) &d_I);
	if (error != CL_SUCCESS)
		fatal_CL(error, __LINE__);

	//====================================================================================================100
	//	End
	//====================================================================================================100

	//======================================================================================================================================================150
	// 	COMPUTATION
	//======================================================================================================================================================150

	printf("Iterations Progress: ");

	// execute main loop
	for (iter = 0; iter < niter; iter++) {// do for the number of iterations input parameter

		printf("%d ", iter);
		fflush(NULL);

		//====================================================================================================100
		// Prepare kernel
		//====================================================================================================100

		// launch kernel
		error = clEnqueueNDRangeKernel(hardware.queue, prepare_software.kernel,
				1, NULL, global_work_size, local_work_size, 0, NULL, NULL);
		if (error != CL_SUCCESS)
			fatal_CL(error, __LINE__);

		// synchronize
		// error = clFinish(hardware.queue);
		// if (error != CL_SUCCESS) 
		// fatal_CL(error, __LINE__);

		//====================================================================================================100
		//	Reduce Kernel - performs subsequent reductions of sums
		//====================================================================================================100

		// initial values
		blocks2_work_size = blocks_work_size;		// original number of blocks
		global_work_size2[0] = global_work_size[0];
		no = Ne;							// original number of sum elements
		mul = 1;										// original multiplier

		// loop% % %
		while (blocks2_work_size != 0) {

			// set arguments that were uptaded in this loop
			error = clSetKernelArg(reduce_software.kernel, 1, sizeof(long),
					(void *) &no);
			if (error != CL_SUCCESS)
				fatal_CL(error, __LINE__);
			error = clSetKernelArg(reduce_software.kernel, 2, sizeof(int),
					(void *) &mul);
			if (error != CL_SUCCESS)
				fatal_CL(error, __LINE__);

			error = clSetKernelArg(reduce_software.kernel, 5, sizeof(int),
					(void *) &blocks2_work_size);
			if (error != CL_SUCCESS)
				fatal_CL(error, __LINE__);

			// launch kernel
			error = clEnqueueNDRangeKernel(hardware.queue,
					reduce_software.kernel, 1, NULL, global_work_size2,
					local_work_size, 0, NULL, NULL);
			if (error != CL_SUCCESS)
				fatal_CL(error, __LINE__);

			// synchronize
			// error = clFinish(hardware.queue);
			// if (error != CL_SUCCESS) 
			// fatal_CL(error, __LINE__);

			// update execution parameters
			no = blocks2_work_size;			// get current number of elements
			if (blocks2_work_size == 1) {
				blocks2_work_size = 0;
			} else {
				mul = mul * NUMBER_THREADS;				// update the increment
				blocks_x = blocks2_work_size / (int) local_work_size[0];// number of blocks
				if (blocks2_work_size % (int) local_work_size[0] != 0) {// compensate for division remainder above by adding one grid
					blocks_x = blocks_x + 1;
				}
				blocks2_work_size = blocks_x;
				global_work_size2[0] = blocks2_work_size
						* (int) local_work_size[0];
			}

		}

		// copy total sums to device
		error = clEnqueueReadBuffer(hardware.queue, d_sums, CL_TRUE, 0,
				mem_size_single, &total, 0, NULL, NULL);
		if (error != CL_SUCCESS)
			fatal_CL(error, __LINE__);

		error = clEnqueueReadBuffer(hardware.queue, d_sums2, CL_TRUE, 0,
				mem_size_single, &total2, 0, NULL, NULL);
		if (error != CL_SUCCESS)
			fatal_CL(error, __LINE__);

		//====================================================================================================100
		// calculate statistics
		//====================================================================================================100

		meanROI = total / (fp) (NeROI);	// gets mean (average) value of element in ROI
		meanROI2 = meanROI * meanROI;										//
		varROI = (total2 / (fp) (NeROI)) - meanROI2;	// gets variance of ROI
		q0sqr = varROI / meanROI2;			// gets standard deviation of ROI

		//====================================================================================================100
		// execute srad kernel
		//====================================================================================================100

		// set arguments that were uptaded in this loop
		error = clSetKernelArg(srad_software.kernel, 12, sizeof(fp),
				(void *) &q0sqr);
		if (error != CL_SUCCESS)
			fatal_CL(error, __LINE__);

		// launch kernel
		error = clEnqueueNDRangeKernel(hardware.queue, srad_software.kernel, 1,
				NULL, global_work_size, local_work_size, 0, NULL, NULL);
		if (error != CL_SUCCESS)
			fatal_CL(error, __LINE__);

		// synchronize
		// error = clFinish(hardware.queue);
		// if (error != CL_SUCCESS) 
		// fatal_CL(error, __LINE__);

		//====================================================================================================100
		// execute srad2 kernel
		//====================================================================================================100

		// launch kernel
		error = clEnqueueNDRangeKernel(hardware.queue, srad2_software.kernel, 1,
				NULL, global_work_size, local_work_size, 0, NULL, NULL);
		if (error != CL_SUCCESS)
			fatal_CL(error, __LINE__);

		// synchronize
		// error = clFinish(hardware.queue);
		// if (error != CL_SUCCESS) 
		// fatal_CL(error, __LINE__);

		//====================================================================================================100
		// End
		//====================================================================================================100

	}

	printf("\n");

	//======================================================================================================================================================150
	// 	Compress Kernel - SCALE IMAGE UP FROM 0-1 TO 0-255 AND COMPRESS
	//======================================================================================================================================================150

	//====================================================================================================100
	// set parameters
	//====================================================================================================100

	error = clSetKernelArg(compress_software.kernel, 0, sizeof(long),
			(void *) &Ne);
	if (error != CL_SUCCESS)
		fatal_CL(error, __LINE__);
	error = clSetKernelArg(compress_software.kernel, 1, sizeof(cl_mem),
			(void *) &d_I);
	if (error != CL_SUCCESS)
		fatal_CL(error, __LINE__);

	//====================================================================================================100
	// launch kernel
	//====================================================================================================100

	error = clEnqueueNDRangeKernel(hardware.queue, compress_software.kernel, 1,
			NULL, global_work_size, local_work_size, 0, NULL, NULL);
	if (error != CL_SUCCESS)
		fatal_CL(error, __LINE__);

	//====================================================================================================100
	// synchronize
	//====================================================================================================100

	error = clFinish(hardware.queue);
	if (error != CL_SUCCESS)
		fatal_CL(error, __LINE__);

	//====================================================================================================100
	//	End
	//====================================================================================================100

	//======================================================================================================================================================150
	// 	COPY RESULTS BACK TO CPU
	//======================================================================================================================================================150

	error = clEnqueueReadBuffer(hardware.queue, d_I, CL_TRUE, 0, mem_size,
			image, 0, NULL, NULL);
	if (error != CL_SUCCESS)
		fatal_CL(error, __LINE__);

	// int i;
	// for(i=0; i<100; i++){
	// printf("%f ", image[i]);
	// }

	//======================================================================================================================================================150
	// 	FREE MEMORY
	//======================================================================================================================================================150

	// OpenCL structures
	error = clReleaseKernel(extract_software.kernel);
	if (error != CL_SUCCESS)
		fatal_CL(error, __LINE__);
	error = clReleaseKernel(prepare_software.kernel);
	if (error != CL_SUCCESS)
		fatal_CL(error, __LINE__);
	error = clReleaseKernel(reduce_software.kernel);
	if (error != CL_SUCCESS)
		fatal_CL(error, __LINE__);
	error = clReleaseKernel(srad_software.kernel);
	if (error != CL_SUCCESS)
		fatal_CL(error, __LINE__);
	error = clReleaseKernel(srad2_software.kernel);
	if (error != CL_SUCCESS)
		fatal_CL(error, __LINE__);
	error = clReleaseKernel(compress_software.kernel);
	if (error != CL_SUCCESS)
		fatal_CL(error, __LINE__);
	error = clReleaseProgram(extract_software.program);
	if (error != CL_SUCCESS)
		fatal_CL(error, __LINE__);
	error = clReleaseProgram(prepare_software.program);
	if (error != CL_SUCCESS)
		fatal_CL(error, __LINE__);
	error = clReleaseProgram(reduce_software.program);
	if (error != CL_SUCCESS)
		fatal_CL(error, __LINE__);
	error = clReleaseProgram(srad_software.program);
	if (error != CL_SUCCESS)
		fatal_CL(error, __LINE__);
	error = clReleaseProgram(srad2_software.program);
	if (error != CL_SUCCESS)
		fatal_CL(error, __LINE__);
	error = clReleaseProgram(compress_software.program);
	if (error != CL_SUCCESS)
		fatal_CL(error, __LINE__);

	// common_change
	error = clReleaseMemObject(d_I);
	if (error != CL_SUCCESS)
		fatal_CL(error, __LINE__);
	error = clReleaseMemObject(d_c);
	if (error != CL_SUCCESS)
		fatal_CL(error, __LINE__);

	error = clReleaseMemObject(d_iN);
	if (error != CL_SUCCESS)
		fatal_CL(error, __LINE__);
	error = clReleaseMemObject(d_iS);
	if (error != CL_SUCCESS)
		fatal_CL(error, __LINE__);
	error = clReleaseMemObject(d_jE);
	if (error != CL_SUCCESS)
		fatal_CL(error, __LINE__);
	error = clReleaseMemObject(d_jW);
	if (error != CL_SUCCESS)
		fatal_CL(error, __LINE__);

	error = clReleaseMemObject(d_dN);
	if (error != CL_SUCCESS)
		fatal_CL(error, __LINE__);
	error = clReleaseMemObject(d_dS);
	if (error != CL_SUCCESS)
		fatal_CL(error, __LINE__);
	error = clReleaseMemObject(d_dE);
	if (error != CL_SUCCESS)
		fatal_CL(error, __LINE__);
	error = clReleaseMemObject(d_dW);
	if (error != CL_SUCCESS)
		fatal_CL(error, __LINE__);

	error = clReleaseMemObject(d_sums);
	if (error != CL_SUCCESS)
		fatal_CL(error, __LINE__);
	error = clReleaseMemObject(d_sums2);
	if (error != CL_SUCCESS)
		fatal_CL(error, __LINE__);

	// OpenCL structures
	error = clFlush(hardware.queue);
	if (error != CL_SUCCESS)
		fatal_CL(error, __LINE__);
	error = clReleaseCommandQueue(hardware.queue);
	if (error != CL_SUCCESS)
		fatal_CL(error, __LINE__);
	error = clReleaseContext(hardware.context);
	if (error != CL_SUCCESS)
		fatal_CL(error, __LINE__);

	//======================================================================================================================================================150
	// 	End
	//======================================================================================================================================================150

}

//========================================================================================================================================================================================================200
//	End
//========================================================================================================================================================================================================200
