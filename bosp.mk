
ifdef CONFIG_CONTRIB_SRADOCL

# Targets provided by this project
.PHONY: sradocl clean_sradocl

# Add this to the "contrib_testing" target
testing: sradocl
clean_testing: clean_sradocl

MODULE_CONTRIB_USER_SRADOCL=contrib/user/SradOCL

sradocl: external
	@echo
	@echo "==== Building SradOCL ($(BUILD_TYPE)) ===="
	@echo " Using GCC    : $(CC)"
	@echo " Target flags : $(TARGET_FLAGS)"
	@echo " Sysroot      : $(PLATFORM_SYSROOT)"
	@echo " BOSP Options : $(CMAKE_COMMON_OPTIONS)"
	@[ -d $(MODULE_CONTRIB_USER_SRADOCL)/build/$(BUILD_TYPE) ] || \
		mkdir -p $(MODULE_CONTRIB_USER_SRADOCL)/build/$(BUILD_TYPE) || \
		exit 1
	@cd $(MODULE_CONTRIB_USER_SRADOCL)/build/$(BUILD_TYPE) && \
		CC=$(CC) CFLAGS=$(TARGET_FLAGS) \
		CXX=$(CXX) CXXFLAGS=$(TARGET_FLAGS) \
		cmake $(CMAKE_COMMON_OPTIONS) ../.. || \
		exit 1
	@cd $(MODULE_CONTRIB_USER_SRADOCL)/build/$(BUILD_TYPE) && \
		make -j$(CPUS) install || \
		exit 1

clean_sradocl:
	@echo
	@echo "==== Clean-up SradOCL Application ===="
	@[ ! -f $(BUILD_DIR)/usr/bin/sradocl ] || \
		rm -f $(BUILD_DIR)/etc/bbque/recipes/SradOCL*; \
		rm -f $(BUILD_DIR)/usr/bin/sradocl*
	@rm -rf $(MODULE_CONTRIB_USER_SRADOCL)/build
	@echo

else # CONFIG_CONTRIB_SRADOCL

sradocl:
	$(warning contib module SradOCL disabled by BOSP configuration)
	$(error BOSP compilation failed)

endif # CONFIG_CONTRIB_SRADOCL

